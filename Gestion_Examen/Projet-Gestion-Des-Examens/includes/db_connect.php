<?php
$db_username = 'root'; // Utilisation du même nom d'utilisateur que dans le conteneur MariaDB
$db_password = ''; // Laisser le mot de passe vide, car il est défini dans le conteneur MariaDB
$db_name     = 'gestion_exam';
$db_host     = 'db'; // Utilisation du nom du service de la base de données comme hôte
$db = mysqli_connect($db_host, $db_username, $db_password, $db_name) or die('could not connect to database');
?>
